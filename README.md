# Discord Nickname Bot

Discord bot to allow users to change each others' nicknames!

## Usage

You can add NickBot to your Discord server using [this link](https://discord.com/api/oauth2/authorize?client_id=819342744073666600&permissions=201328640&scope=bot)!

The bot responds to the following commands:
- `!nick <user> <new nick>` - set the nickname for the specified user (must be `@User`)
- `!help` - display a help message in chat

## Installation

To run the bot yourself, you must create a Discord bot at the [development portal](https://discordapp.com/developers/applications/).
You will also need to have a recent version of Node.js.

Clone the repo and install prerequisites:
```
git clone https://gitlab.com/jsmailes/discord-nickname-bot.git
cd discord-nickname-bot
npm install
```

Next you will need to edit `config.json` so that `token` is set to your bot's token.

Now we can run the bot!
```
node index.js
```

## Running as a systemd service

TBA
