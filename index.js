const Discord = require("discord.js");
const { prefix, token } = require("./config.json");

const client = new Discord.Client();

client.once("ready", () => {
    console.log("Ready!");
});

client.once("reconnecting", () => {
    console.log("Reconnecting!");
});

client.once("disconnect", () => {
    console.log("Disconnect!");
});

client.on("message", async message => {
    if (message.author.bot) return;
    if (!message.content.startsWith(prefix)) return;

    if (message.content.startsWith(`${prefix}nick`)) {
        nick(message);
        return;
    } else if (message.content.startsWith(`${prefix}help`)) {
        message.channel.send({
            embed: {
                color: 11361238,
                title: "Available commands:",
                fields: [
                    {
                        name: "Command",
                        value: "nick @<user> <new nick>\nhelp",
                        inline: true
                    },
                    {
                        name: "Result",
                        value: "Change the nickname for <user> to the specified nick\nSend this message",
                        inline: true
                    }
                ]
            }
        });
    }
});

function nick(message) {
    console.log(message.content);
    const args = message.content.split(" ");

    if (args.length < 3) {
        return message.channel.send(
            "Usage: " + prefix + "nick @<user> <new nick>."
        );
    }

    args_joined = args.splice(0, 2);
    args_joined.push(args.join(" "));

    username = args_joined[1];
    new_nick = args_joined[2];
    console.log(username);
    console.log(new_nick);

    // look up user
    client.users.fetch(username.substring(3, username.length - 1))
    .then(function(user) {
        console.log(user);
        member = message.guild.member(user);
        console.log(member);

        member.setNickname(new_nick)
        .then(function() {
            message.channel.send(
                "Changed nickname for " + user.username + " to " + new_nick + "!"
            );
        })
        .catch(function() {
            message.channel.send(
                "Could not change nickname for " + user.username + "."
            );
        });
    })
    .catch(function() {
        message.channel.send(
            "Couldn't find user " + username + ". Try @ing the user instead!"
        );
    });
}

client.login(token);
